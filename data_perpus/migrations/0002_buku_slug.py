# Generated by Django 2.2 on 2020-04-18 17:27

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('data_perpus', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='buku',
            name='slug',
            field=models.SlugField(default=django.utils.timezone.now, editable=False, max_length=255, unique=True),
            preserve_default=False,
        ),
    ]
