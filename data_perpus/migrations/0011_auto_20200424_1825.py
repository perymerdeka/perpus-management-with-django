# Generated by Django 2.2 on 2020-04-24 18:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_perpus', '0010_auto_20200424_0250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buku',
            name='gambar',
            field=models.ImageField(blank=True, default='/static/assets/img/no-pic.png', null=True, upload_to='gambar_buku'),
        ),
    ]
