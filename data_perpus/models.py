# import modul yang dibutuhkan
from django.db import models
from django.contrib.auth.models import User # digunakan untuk import User
from django.urls import reverse # digunakan untuk meng-generate url dengan reverse URLpatterns
from django.db.models.signals import post_save
from django.utils.text import slugify
# Create your models here.

# relasi database untuk mengatur genre buku
class Genre(models.Model):
    genre = models.CharField(max_length=200, help_text="Masukan Genre Buku, (Misalnya: Kesehatan, Sains, Teknologi)")

    def get_absolute_url(self):
        return reverse('index')
    # mengembalikan nilai string dan dikembalikan dengan object menggunakan __str__
    def __str__(self):
        return self.genre

# relasi database untuk memanagemen bahasa buku yang akan dipinjam
class BahasaBuku(models.Model):
    bahasa = models.CharField(max_length=200, help_text="Masukan Bahasa Buku (misalnya: Bahasa Indonesia, English, Jepang)")

    def get_absolute_url(self):
        return reverse('index')

    def __str__(self):
        return self.bahasa

# relasi buku akan mempunyai 2 foreign key yaitu author
# relasi buku dapat menampung genre jadi menggunakan manytomany field
class Buku(models.Model):
    judul = models.CharField(max_length=200)
    author = models.CharField(max_length=150)
    slug = models.SlugField(max_length=255,unique=True, editable=False)
    deskripsi = models.TextField(help_text="Masukan Deskripsi Buku")
    isbn = models.CharField(max_length=13, help_text='13 Karakter <a href="https://www.isbn-international.org/content/what-isbn">Nomor ISBN</a>')
    genre = models.ManyToManyField(Genre, help_text="Apa Genre untuk Buku ini")
    bahasa = models.ForeignKey('BahasaBuku', on_delete=models.SET_NULL, null=True)
    total_copies = models.IntegerField()
    available_copies = models.IntegerField()
    gambar = models.ImageField(blank=True, null=True, upload_to='gambar_buku/')

    @property
    def get_gambar_url(self):
        if self.gambar and hasattr(self.gambar, 'url'):
            return self.gambar.url
        else:
            return "/media/assets/no-pic.png"

    def save(self):
        self.slug = slugify(self.judul)
        super(Buku, self).save()

    def get_absolute_url(self):
        url_slug = {
            'slug':self.slug,
        }

        return reverse('buku:detail-buku', kwargs=url_slug)


    def __str__(self):
        return self.judul


# relasi database Siswa digunakan untuk menampung info tentang Siswa yang meminjam buku di perpus
# fungsi nisn digunakan untuk mengidentifikasi siswa secara unik
class Siswa(models.Model):
    CHOICES_JURUSAN = (
        ('Teknik Komputer Jaringan', 'Teknik Komputer Jaringan'),
        ('Teknik Kendaraan Ringan', 'Teknik Kendaraan Ringan'),
        ('Kriya Tekstil', 'Kriya Tekstil'),
        ("Perbankan Syari'ah", "Perbankan Syari'ah"),
    )

    CHOICES_KELAS = (
        ('X', 'X'),
        ('XI', 'XI'),
        ('XII', 'XII'),
    )


    nisn = models.CharField(max_length=10, unique=True)
    nama = models.CharField(max_length=100)
    kelas = models.CharField(max_length=3, choices=CHOICES_KELAS, default='x')
    jurusan = models.CharField(max_length=100, choices=CHOICES_JURUSAN, default='tkj')
    nomor_telp = models.CharField(max_length=14)
    total_buku = models.IntegerField(default=0)
    email = models.EmailField(unique=True)
    foto = models.ImageField(blank=True, upload_to='foto_profile')

    def get_absolute_url(self):
        url_id = {
            'url_id': self.id,
            }
        return reverse('buku:list-siswa')


    def __str__(self):
        return str(self.nama)


# relasi digunakan untuk menginfo kan tentang buku yang dipinjam
# ForeignKey buku dan siswa untuk mereferensi buku dan Siswa
# fungsi roll_no digunakan untuk identifikasi Siswa
# jika buku dikembalikan maka akan data pinjaman akan dihapus dari database

class PinjamBuku(models.Model):
    siswa = models.ForeignKey('Siswa', on_delete=models.CASCADE)
    buku = models.ForeignKey('Buku', on_delete=models.CASCADE)
    awal_pinjam = models.DateTimeField(null=True, blank=True, help_text="Format Tanggal: tahun-bulan-tanggal, contoh:2020-05-03")
    tgl_kembali = models.DateTimeField(null=True, blank=True, help_text="Format Tanggal: tahun-bulan-tanggal, contoh:2020-05-03")

    def get_absolute_url(self):
        return reverse('buku:list-pinjam')

    def __str__(self):
        return self.siswa.nama+" telah meminjam "+self.buku.judul
