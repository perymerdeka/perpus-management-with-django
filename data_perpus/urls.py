# import modul yang dibutuhkan
from django.conf.urls import url
from django.urls import path

# import views untuk ditampilkan
from . import views
from .models import Siswa
app_name = 'buku'

urlpatterns = [
    path('tambah-bahasa-buku/', views.BahasaBukuCreateView.as_view(),name='tambah-bahasa-buku'),
    path('tambah-genre/', views.GenreCreateView.as_view(),name='tambah-genre'),
    path('delete-pinjam-buku/<int:pk>', views.PinjamDeleteView.as_view(), name='pinjam-delete'),
    path('update-pinjam-buku/<int:pk>', views.PinjamUpdateView.as_view(),name='pinjam-update'),
    path('pinjam-buku/', views.PinjamCreateView.as_view(), name='pinjam-buku'),
    path('list-pinjam/', views.PinjamListView.as_view(), name='list-pinjam'),
    path('delete-siswa/<int:pk>', views.SiswaDeteteView.as_view(),name='delete-siswa'),
    path('update-siswa/<int:pk>', views.SiswaUpdateView.as_view(), name='update-siswa'),
    path('tambah-siswa/', views.SiswaCreateView.as_view(),name='tambah-siswa'),
    path('detail-siswa/<int:pk>', views.SiswaDetailView.as_view(), name='detail-siswa'),
    path('list-siswa', views.SiswaListView.as_view(),name='list-siswa'),
    path('delete-buku/<int:pk>/', views.BookDeleteView.as_view(),name='delete-buku'),
    path('update-buku/<int:pk>/', views.BookUpdateView.as_view(),name='update-buku'),
    path('tambah-buku/', views.BookCreateView.as_view(), name='tambah-buku'),
    path('detail-buku/<slug:slug>', views.BookDetailView.as_view(), name='detail-buku'),
    path('', views.BookListView.as_view(), name='list'),
]
