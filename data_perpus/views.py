# modul utama yang dibutuhkan
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy

# modul untuk menginclude metod decorator pada class
from django.utils.decorators import method_decorator

# modul autentifikasi decorator
from django.contrib.auth.decorators import login_required

# model untuk menentukan waktu
import datetime

# modul untuk template
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

# modul untuk kebutuhan relasi database dan forms untuk ditampilkan
from .models import *
from . forms import *


# class Buku
class BookListView(ListView):
    model = Buku
    template_name = "data_perpus/buku_list.html"
    # context_object_name = 'book_list'

    extra_context = {
        'title':'Semua Buku',
    }

    def get_context_data(self, *args, **kwargs):
        self.kwargs.update(self.extra_context)
        kwargs = self.kwargs
        return super().get_context_data(*args, **kwargs)

# class ini digunakan untuk menampilkan isi dari buku
class BookDetailView(DetailView):
    model = Buku
    template_name = "data_perpus/buku_detail.html"


# class ini digunakan untuk Tambah Buku menggunakan createView
@method_decorator(login_required, name='dispatch')
class BookCreateView(CreateView):
    form_class = FormBuku
    template_name = 'data_perpus/tambah-buku.html'
    extra_context = {
        'judul': 'Tambah Buku',
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)

method_decorator(login_required, name='dispatch')
class BookUpdateView(UpdateView):
    form_class = FormBuku
    model = Buku
    template_name = 'data_perpus/tambah-buku.html'
    extra_context = {
        'judul':'Update Buku'
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)


method_decorator(login_required, name='dispatch')
class BookDeleteView(DeleteView):
    model = Buku
    success_url = reverse_lazy('buku:list')


# class Siswa
class SiswaListView(ListView):
    model = Siswa
    template_name = 'data_perpus/list_siswa.html'
    context_object_name = 'siswa_list'
    extra_context = {
        'title':'Daftar Siswa Anggota Perpus',
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)

method_decorator(login_required, name='dispatch')
class SiswaDetailView(DetailView):
    model = Siswa
    template_name = 'data_perpus/siswa_detail.html'

method_decorator(login_required, name='dispatch')
class SiswaCreateView(CreateView):
    form_class = FormSiswa
    template_name = 'data_perpus/tambah-buku.html'
    extra_context = {
        'judul': 'Tambah Siswa Baru'
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)

method_decorator(login_required, name='dispatch')
class SiswaUpdateView(UpdateView):
    form_class = FormSiswa
    model = Siswa
    template_name = 'data_perpus/tambah-buku.html'
    extra_context = {
        'judul': 'Update Profil Anggota',
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)

method_decorator(login_required, name='dispatch')
class SiswaDeteteView(DeleteView):
    model = Siswa
    success_url = reverse_lazy('buku:list-siswa')


# class Pinjam buku

# class ini digunakan untuk menampilkan siswa yang telah meminjam buku
method_decorator(login_required, name='dispatch')
class PinjamListView(ListView):
    model = PinjamBuku
    template_name = 'data_perpus/list-pinjam.html'
    context_object_name = 'pinjam_buku_list'
    extra_context = {
        'judul':'Daftar Siswa yang Meminjam Buku'
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)

# class ini digunakan untuk meminjam buku
method_decorator(login_required, name='dispatch')
class PinjamCreateView(CreateView):
    form_class = FormPinjamBuku
    model = PinjamBuku
    template_name = 'data_perpus/pinjam-buku.html'
    extra_context = {
        'judul':'Pinjam Buku',
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)

# class Ini digunakan untuk update siswa yang telah meminjam buku
method_decorator(login_required, name='dispatch')
class PinjamUpdateView(UpdateView):
    form_class = FormPinjamBuku
    model = PinjamBuku
    template_name = 'data_perpus/pinjam-buku.html'
    extra_context = {
        'judul':'Update Siswa yang Meminjam Buku',
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)


method_decorator(login_required, name='dispatch')
class PinjamDeleteView(DeleteView):
    model = PinjamBuku
    success_url = reverse_lazy('buku:list-pinjam')


# Class Untuk menambahkan genre
method_decorator(login_required, name='dispatch')
class GenreCreateView(CreateView):
    form_class = GenreBukuForm
    model = Genre
    template_name = 'data_perpus/partials/create_gen.html'
    extra_context = {
        'judul':'Tambah Genre Buku',
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)

# class Untuk menambah bahasa buku
method_decorator(login_required, name='dispatch')
class BahasaBukuCreateView(CreateView):
    form_class = BahasaBukuForm
    model = BahasaBuku
    template_name = 'data_perpus/partials/create_gen.html'
    extra_context = {
        'judul':'Tambah Bahasa Buku',
    }

    def get_context_data(self, *args, **kwargs):
        kwargs.update(self.extra_context)
        return super().get_context_data(*args, **kwargs)
