# import logging
from django import forms
from .models import *
# import app timepicker
from tempus_dominus.widgets import DatePicker, TimePicker, DateTimePicker

class GenreBukuForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = '__all__'
        exclude = ()

class BahasaBukuForm(forms.ModelForm):
    class Meta:
        model = BahasaBuku
        fields = '__all__'
        exclude = ()

class FormBuku(forms.ModelForm):
    class Meta:
        model = Buku
        fields = '__all__'
        exclude = ()


    # def save(self, *args, **kwargs):
    #     logger = logging.getLogger(__name__)
    #     logger.info(self.gambar)
    #     return super(FormBuku, self).save(*args, **kwargs)

    # def clean(self):
    #     cleaned_data = super().clean()
    #     gambar = cleaned_data.get('gambar')
    #     logger = logging.getLogger(__name__)
    #     logger.info(gambar)
    #     return cleaned_data

class FormPinjamBuku(forms.ModelForm):
    class Meta:
        model = PinjamBuku
        fields = [
            'siswa',
            'buku',
            'awal_pinjam',
            'tgl_kembali',
        ]



class FormSiswa(forms.ModelForm):
    class Meta:
        model = Siswa
        fields = '__all__'
