from django.contrib import admin

class PostAdmin(admin.ModelAdmin):
    readonly_fields = ['slug',]


# import models
from . models import *
# Register your models here.
admin.site.register(Buku, PostAdmin)
admin.site.register(Siswa)
admin.site.register(PinjamBuku)
admin.site.register(Genre)
admin.site.register(BahasaBuku)
