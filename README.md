##untuk menjalankan project hal perlu dibutuhkan adalah
- Python 3.6 or higher
- Text Editor (Sublime Text or etc)
- Virtual Environtment 

## cara install virtual Environtment
 ``` python 
 python3.8 -m venv Environtment

 ```
 sesuaikan dengan versi python anda.
 
 lalu aktifkan virtual environtment dengan perintah
 ```bash
 
 source Environtment/bin/activate
 ```
 
 ingat sesuaikan dengan nama environtment yang anda buat
 
 ### Install Paket yang dibutuhkan
 
 seluruh paket yang dibutuhkan ada di requirements.txt install semua dengan menggunakan pip
 ```python
 pip install -r requirements.txt
 ```
 
 ### Menjalankan project
 1. jalankan perintah python manage.py migrate
 
 ```python
 python manage.py migrate
```

2. buat user untuk admin perpus
```python
python manage.py createsuperuser
```
3. jalankan project dengan perintah 
```python
python manage.py runserver
```

 